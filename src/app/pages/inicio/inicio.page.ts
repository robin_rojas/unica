import { Component, OnInit, ViewChild } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ModalController, MenuController, ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { StreamingMedia, StreamingAudioOptions } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  // iconplay: boolean = true;
  // iconpausa: boolean = false;
  // audio = new Audio('http://serviaudio.com:7082/;');
  
  constructor(public nativeAudio: NativeAudio,
              private androidPermissions: AndroidPermissions,
              public modalController: ModalController,
              public menuCtrl: MenuController,
              public toastController: ToastController,
              private streamingmedia: StreamingMedia) {
  }

  ngOnInit() {
  }

  play(){
    // this.iconplay = true;
    // if(this.iconplay == true){
      let options: StreamingAudioOptions = {
        successCallback: () => {console.log()},
        errorCallback: () => {console.log()},
        initFullscreen: false,
        bgImage: "www/assets/unicaicono.png",  
        bgImageScale: "stretch",
      }
      this.streamingmedia.playAudio('http://serviaudio.com:7082/;', options);
      // this.iconplay = false;
      // this.iconpausa = true;
    //}
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(
    //   result => console.log('Has permission?',result.hasPermission),
    //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS)
    // );
    // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    // if(this.iconplay == true){
    //   this.audio.play();
    //   this.iconplay = false;
    //   this.iconpausa = true;
    // }    
  }

  // pausa(){
  //   this.iconpausa = true;
  //   if(this.iconpausa == true){
  //     this.streamingmedia.stopAudio();
  //     this.iconplay = true;
  //     this.iconpausa = false;
  //   }    
  // }

}
