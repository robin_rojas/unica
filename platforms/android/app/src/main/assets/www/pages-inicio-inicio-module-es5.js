(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-inicio-inicio-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/inicio/inicio.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/inicio/inicio.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n\n\n<ion-content>\n\n         <!-- <ion-list style=\"background-image: url('../assets/unicaimg.jpeg'); background-size: cover; \n  height: 100%; width: 100%; overflow-x:hidden; overflow-y:hidden;\"> -->\n\n   <!-- <ion-col>\n    <img style=\"margin-top:10%;\" src=\"assets/logoUnica.jpeg\">\n  </ion-col> -->\n\n  <img style=\"width: 100%;height: 100%;\" src=\"../assets/unicaimg.jpeg\">\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" (click)=\"play()\">\n    <ion-fab-button class=\"start-btn\" color=\"danger\">\n      <ion-icon class=\"iconSize\" style=\"color: white\" ios=\"ios-radio\" md=\"md-radio\"></ion-icon>\n    <!-- <ion-icon class=\"iconSize\" style=\"color: white\" *ngIf=\"iconplay == true\" (click)=\"play()\" ios=\"ios-play\" md=\"md-play\"></ion-icon>\n    <ion-icon class=\"iconSize\" style=\"color: white\" *ngIf=\"iconpausa == true\" (click)=\"pausa()\" ios=\"ios-pause\" md=\"md-pause\"></ion-icon> -->\n    </ion-fab-button>\n  </ion-fab>\n\n  <!-- </ion-list> -->\n\n      \n            \n</ion-content>\n\n\n"

/***/ }),

/***/ "./src/app/pages/inicio/inicio.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/inicio/inicio.module.ts ***!
  \***********************************************/
/*! exports provided: InicioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPageModule", function() { return InicioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _inicio_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./inicio.page */ "./src/app/pages/inicio/inicio.page.ts");







var routes = [
    {
        path: '',
        component: _inicio_page__WEBPACK_IMPORTED_MODULE_6__["InicioPage"]
    }
];
var InicioPageModule = /** @class */ (function () {
    function InicioPageModule() {
    }
    InicioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_inicio_page__WEBPACK_IMPORTED_MODULE_6__["InicioPage"]],
        })
    ], InicioPageModule);
    return InicioPageModule;
}());



/***/ }),

/***/ "./src/app/pages/inicio/inicio.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/inicio/inicio.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".start-btn {\n  width: 90px;\n  height: 90px;\n  margin-bottom: 20px;\n  margin-right: 20px;\n}\n\n.iconSize {\n  font-size: 50px;\n}\n\n.scroll {\n  overflow-x: scroll;\n  overflow-y: hidden;\n}\n\n.noticiaItem {\n  font-size: 14px;\n  background: white;\n  color: black;\n  padding: 3%;\n  text-transform: capitalize;\n  position: absolute;\n  width: 90%;\n  bottom: 10%;\n  margin-left: 3%;\n  margin-right: 3%;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n}\n\n.new {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  width: 35%;\n  margin-top: 10%;\n}\n\n.icono {\n  position: absolute;\n  font-size: 800%;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 80%;\n  text-align: center;\n}\n\n.ico {\n  position: absolute;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 80%;\n  text-align: center;\n  font-size: 800%;\n}\n\n.audio {\n  position: absolute;\n  font-size: 800%;\n  left: 0px;\n  right: 0px;\n  margin: 0px auto;\n  margin-top: 140%;\n  text-align: center;\n}\n\n.img {\n  display: block;\n  width: 27%;\n  margin: 0px auto;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 4%;\n}\n\n.start-btn {\n  width: 100px;\n  height: 100px;\n}\n\n.iconSize {\n  font-size: 70px;\n}\n\n.iconFavorito {\n  position: absolute;\n  margin-top: 2%;\n  margin-left: 2%;\n  font-size: 22px;\n  color: #f7f004;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9yb2JpbnJvamFzL3dlYkFwcC9hcHAvdW5pY2Evc3JjL2FwcC9wYWdlcy9pbmljaW8vaW5pY2lvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaW5pY2lvL2luaWNpby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0M7RUFDRyxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNBSjs7QURHRTtFQUNFLGVBQUE7QUNBSjs7QURHRTtFQUNHLGtCQUFBO0VBQ0Esa0JBQUE7QUNBTDs7QURHRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBRUEsMENBQUE7QUNESjs7QURJQTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNESjs7QURJQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUVBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNGSjs7QURLQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNGSjs7QURLQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUVBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDSEo7O0FET0E7RUFDSSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNKSjs7QURPQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FDSko7O0FET0U7RUFDRSxlQUFBO0FDSko7O0FET0E7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0pKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaW5pY2lvL2luaWNpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbiAuc3RhcnQtYnRuIHtcbiAgICB3aWR0aDogOTBweDtcbiAgICBoZWlnaHQ6IDkwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIH1cblxuICAuaWNvblNpemUge1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgfVxuXG4gIC5zY3JvbGx7XG4gICAgIG92ZXJmbG93LXg6c2Nyb2xsOyBcbiAgICAgb3ZlcmZsb3cteTpoaWRkZW47XG4gIH0gIFxuXG4gIC5ub3RpY2lhSXRlbXtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LDEpO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBwYWRkaW5nOiAzJTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBib3R0b206IDEwJTtcbiAgICBtYXJnaW4tbGVmdDogMyU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjY2NjY2NjNGQ7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG59XG5cbi5uZXd7XG4gICAgZGlzcGxheTpibG9jaztcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDM1JTtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbi5pY29ub3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1zaXplOiA4MDAlO1xuICAgIC8vIGNvbG9yOiBibGFjaztcbiAgICBsZWZ0OiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDgwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxufVxuXG4uaWNve1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDgwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiA4MDAlO1xufVxuXG4uYXVkaW97XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtc2l6ZTogODAwJTtcbiAgICAvLyBjb2xvcjogYmxhY2s7XG4gICAgbGVmdDogMHB4O1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgbWFyZ2luOiAwcHggYXV0bztcbiAgICBtYXJnaW4tdG9wOiAxNDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG4uaW1ne1xuICAgIGRpc3BsYXk6YmxvY2s7XG4gICAgd2lkdGg6IDI3JTtcbiAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLnN0YXJ0LWJ0biB7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGhlaWdodDogMTAwcHg7XG4gIH1cblxuICAuaWNvblNpemUge1xuICAgIGZvbnQtc2l6ZTogNzBweDtcbiAgfVxuXG4uaWNvbkZhdm9yaXRve1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tdG9wOiAyJTtcbiAgICBtYXJnaW4tbGVmdDogMiU7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIGNvbG9yOiAjZjdmMDA0O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyAgXG59IiwiLnN0YXJ0LWJ0biB7XG4gIHdpZHRoOiA5MHB4O1xuICBoZWlnaHQ6IDkwcHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuLmljb25TaXplIHtcbiAgZm9udC1zaXplOiA1MHB4O1xufVxuXG4uc2Nyb2xsIHtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG59XG5cbi5ub3RpY2lhSXRlbSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiBibGFjaztcbiAgcGFkZGluZzogMyU7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA5MCU7XG4gIGJvdHRvbTogMTAlO1xuICBtYXJnaW4tbGVmdDogMyU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLm5ldyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB3aWR0aDogMzUlO1xuICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbi5pY29ubyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiA4MDAlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDgwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaWNvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDgwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDgwMCU7XG59XG5cbi5hdWRpbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiA4MDAlO1xuICBsZWZ0OiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIG1hcmdpbi10b3A6IDE0MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmltZyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMjclO1xuICBtYXJnaW46IDBweCBhdXRvO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLnN0YXJ0LWJ0biB7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbn1cblxuLmljb25TaXplIHtcbiAgZm9udC1zaXplOiA3MHB4O1xufVxuXG4uaWNvbkZhdm9yaXRvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAyJTtcbiAgbWFyZ2luLWxlZnQ6IDIlO1xuICBmb250LXNpemU6IDIycHg7XG4gIGNvbG9yOiAjZjdmMDA0O1xuICBmb250LXdlaWdodDogYm9sZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/inicio/inicio.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/inicio/inicio.page.ts ***!
  \*********************************************/
/*! exports provided: InicioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPage", function() { return InicioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/native-audio/ngx */ "./node_modules/@ionic-native/native-audio/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/streaming-media/ngx */ "./node_modules/@ionic-native/streaming-media/ngx/index.js");






var InicioPage = /** @class */ (function () {
    // iconplay: boolean = true;
    // iconpausa: boolean = false;
    // audio = new Audio('http://serviaudio.com:7082/;');
    function InicioPage(nativeAudio, androidPermissions, modalController, menuCtrl, toastController, streamingmedia) {
        this.nativeAudio = nativeAudio;
        this.androidPermissions = androidPermissions;
        this.modalController = modalController;
        this.menuCtrl = menuCtrl;
        this.toastController = toastController;
        this.streamingmedia = streamingmedia;
    }
    InicioPage.prototype.ngOnInit = function () {
    };
    InicioPage.prototype.play = function () {
        // this.iconplay = true;
        // if(this.iconplay == true){
        var options = {
            successCallback: function () { console.log(); },
            errorCallback: function () { console.log(); },
            initFullscreen: false,
            bgImage: "www/assets/unicaicono.png",
            bgImageScale: "stretch",
        };
        this.streamingmedia.playAudio('http://serviaudio.com:7082/;', options);
        // this.iconplay = false;
        // this.iconpausa = true;
        //}
        // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(
        //   result => console.log('Has permission?',result.hasPermission),
        //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS)
        // );
        // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
        // if(this.iconplay == true){
        //   this.audio.play();
        //   this.iconplay = false;
        //   this.iconpausa = true;
        // }    
    };
    InicioPage.ctorParameters = function () { return [
        { type: _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_2__["NativeAudio"] },
        { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__["AndroidPermissions"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_5__["StreamingMedia"] }
    ]; };
    InicioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inicio',
            template: __webpack_require__(/*! raw-loader!./inicio.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/inicio/inicio.page.html"),
            styles: [__webpack_require__(/*! ./inicio.page.scss */ "./src/app/pages/inicio/inicio.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_2__["NativeAudio"],
            _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__["AndroidPermissions"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_native_streaming_media_ngx__WEBPACK_IMPORTED_MODULE_5__["StreamingMedia"]])
    ], InicioPage);
    return InicioPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-inicio-inicio-module-es5.js.map